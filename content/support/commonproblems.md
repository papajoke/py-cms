+++
  date = "2016-09-05T21:03:22+02:00"
  title = "Common Problems"
  type = "support-commonproblems"
  weight = 1
  right = false
  navsection = "support"
  template = "list.html"
+++

Almost nothing is without problems and while our team and our community works hard to get rid of all problems they encounter, some might slip through the cracks or are caused by some external influences. Here are some common problems that other users encountered.

If you encountered something that is not listed here, You can always visit the [Forum](https://forum.manjaro.org). Point out bugs or discuss with other users and try to find a solution.
