"""
 functions in template
"""
#import jinja2
import urllib.request, urllib.error
import json

def plugin(aobject, *args):
    """ 
    usage: {{ object | plugin(method_name)
    not usefull can use:
    {% if curdate is defined %}{{curdate.html()|safe }}{% endif %}
    """

    if not aobject:
        return "plugin not exists empty !"
    afunc = ""
    #for a in args:
    #    print(":" + a)
    if len(args) > 0:
        afunc = args[0]
        args = args[1:]
        if len(args) == 1:
            args = args[0]
    try:
        #print(f"{dir(aobject)} -> {afunc}")
        method = getattr(aobject, afunc)
        if args:
            return method(args)
        else:
            return method()
    except:
        return f" exception ? {afunc}"

def httpget(url):
    try:
        return json.load(urllib.request.urlopen(url))
    except urllib.error.HTTPError:
        pass
    return {}
