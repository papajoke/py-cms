"""
 here only one model ... ?
"""
from pathlib import Path
import markdown
#import yaml
#from yaml import dump
import toml
import flask

class Pages(dict):
    """ all pages in cms """
    def childrens(self, page):
        for _, subpage in self.items():
            if subpage.parent == page.slug:
                yield subpage

    def append(self, page):
        papa = "/".join(page.slug.split('/')[:-1])
        try:
            page.parent = self[papa].slug
        except KeyError:
            page.parent = "/"
        #self.pages[page.slug] = data
        self[page.slug] = page

        return self


class Model():
    """ abstract page content """
    def __init__(self, pfile: object, baseroot: str):
        """ class initialisation """
        self.filename = pfile
        self.type = pfile.suffix
        long = len(str(Path(baseroot+'/../content').resolve()))
        self.slug = f"{pfile.parent}/{pfile.stem}"[long+1:].strip()
        #print(self.filename)
        #print(self.slug)
        self.title = ""
        self.content = ""
        self.template = "body.html"
        self.items = {}
        self.parent = "/"
        content = pfile.read_text()
        self.parse(content)

    def load(self, afile: str):
        pass

    def parse(self, content: str):
        self.title = ""
        self.items = {}
        self.content = ""

        try:
            par = str(content.split("+++\n", 3)[1])
            datas = toml.loads(par)
            toml.dumps(datas)
            self.items = datas
            try:
                self.title = self.items['title']
            except KeyError:
                pass
            try:
                self.template = self.items['template']
            except KeyError:
                pass
        except IndexError:
            pass

    def load_content(self) ->str:
        self.content = self.filename.read_text()
        pos = self.content.find("+++\n", 4)
        self.content = self.content[pos+4:]
        return self.content

    def childrens(self, pages) ->list:
        pages = (p for p in pages.items() if p.slug.startswith(self.slug))
        for page in pages:
            yield page

    def render(self, **context) ->str:
        """ we can use jinja in content !! """
        if not self.content:
            self.load_content()
        template = markdown.markdown(self.content, output_format="html5", extensions=['extra'])
        return flask.render_template_string(template, **context)
