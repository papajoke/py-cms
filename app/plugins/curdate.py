"""
  test simple plugin
"""
from datetime import date

DATA: str = ""

def init():
    """ initialisation """
    global DATA
    DATA = str(date.today())
    return DATA

def html():
    """ output """
    return f"test plugin Curdate: {DATA}"
