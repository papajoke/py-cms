"""
  test simple plugin
"""
import os

DATA: str = ""

def init():
    """ initialisation """
    global DATA
    DATA = os.getenv("PATH")
    return DATA

def html():
    """ output """
    return f"test plugin Path: {DATA}"
