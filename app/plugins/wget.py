"""
  test simple plugin
"""
import urllib.request #, urllib.error
import json

DATA: dict = {}

def init():
    """ initialisation """
    return DATA

def getjson() ->dict:
    """ output """
    return DATA

def load(url, persistant=True):
    if persistant:
        global DATA
        if DATA:
            return DATA # load only one time
        DATA = {}
        try:
            DATA = json.load(urllib.request.urlopen(url))
        except:
            pass
        return DATA
    else:
        try:
            return json.load(urllib.request.urlopen(url))
        except:
            return {}
