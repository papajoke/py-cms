from pathlib import Path
from types import resolve_bases

class TemplateManager:
    def __init__(self, directory: str):
        """ class initialisation """
        self.default = "body.html"
        self.directory = f"{directory}/../templates"

    def get(self, param=None, default: str = None) -> str:
        print(Path(f"{self.directory}/{param}").resolve())
        if not default:
            default = self.default
        if param:
            if Path(f"{self.directory}/{param}").exists():
                return param
        return self.default
