PLUGINS = {}

def load_plugin(name: str):
    """import one module if exist"""
    try:
        return __import__(f"app.plugins.{name}", globals(), locals(), [name], 0)
    except ModuleNotFoundError:
        pass
    return None


def find_plugin(items: list):
    global PLUGINS
    if not items:
        return
    if not isinstance(items, list):  # if ites id only one str
        items = [items]
    for plugin_name in items:
        if plugin_name in PLUGINS:
            continue    # load only one
        mod = load_plugin(plugin_name)
        if mod:
            mod.init()
            PLUGINS[plugin_name] = mod
        print(f"loaded: {plugin_name}: {mod.__name__}  {mod.__file__}")


def injectplugins(datas: dict) ->dict:
    for k, value in PLUGINS.items():
        datas[k] = value
    return datas
