import os
from flask import Flask
from .routes import app
from .config import MainConfig

baseroot = os.path.abspath(f"{os.path.dirname(__file__)}/../www")
print("app._init__ root:", baseroot)


def loadconfig(app):
    app.config["blog"] = MainConfig(baseroot)
    app.config["blog"].scan()
    app.config["blog"].load_global()
    #print("app.__init__:", app.__dict__)

if not app.config.get("blog"):
    loadconfig(app)
