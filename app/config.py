from pathlib import Path
import toml
from .models._model import Model
from .models._model import Pages

class MainConfig():
    """ configurations """
    def __init__(self, baseroot: str):
        """ class initialisation """
        self.root = baseroot
        self.debug = False
        self.testing = False
        self.development = False
        self.secret_key = "this-really-needs-to-be-changed"
        self.site = {'pages':{}}
        self.pages = Pages()

    def scan(self):
        """ index all pages """
        for page in Path(f"{self.root}/../content").resolve().glob('**/*.md'):
            if page.is_dir():
                continue
            try:
                self.pages.append(
                    Model(page, self.root)
                )
            except toml.decoder.TomlDecodeError as err:
                print(f"\nWARNING: toml invalide {page}\n  ", err)

    def load_global(self):
        """ load site configuration """
        fconfig = Path(f"{self.root}/../content/config.toml").resolve()
        self.site = toml.load(str(fconfig))
        self.site['pages'] = self.pages
        #print(self.site)
