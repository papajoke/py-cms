from pathlib import Path
from flask import Flask
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import abort
from . import templatemanager
from . import pluginmanager
from . import templatehooks

app = Flask(
    __name__,
    #root_path=f"{app.root_path}/..",
    template_folder="../templates",
    static_url_path='/static'
)
app.root_path = f"{app.root_path}/../www"
app.jinja_env.filters['plugin'] = templatehooks.plugin
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True


try:
    app.config.from_envvar('FLASK_CMS_SETTINGS')    # https://flask.palletsprojects.com/en/1.1.x/config/#configuring-from-files
except RuntimeError as err:
    app.config['DEBUG'] = True    # for moment debug is default
    print(err)
#print(f"routes.py: {app.__dict__=}")

@app.route('/')
def gohome():
    return catch_all("_index")

@app.route('/raz')
def raz():
    """ re-load config """
    #TODO if cache : clear
    from . import loadconfig
    loadconfig(app)
    return gohome()

@app.route("/img/<img_name>")
@app.route("/images/<img_name>")
def get_image(img_name):
    try:
        return send_from_directory(f"{app.root_path}/static/img", filename=img_name, as_attachment=False)
    except FileNotFoundError:
        abort(404)

@app.route("/fonts/<font_name>")
def get_font(font_name):
    try:
        return send_from_directory(f"{app.root_path}/static/fonts", filename=font_name, as_attachment=False)
    except FileNotFoundError:
        abort(404)

@app.route('/myApp')
def myPythonApplication():
    return "a classic flask app...for example discover"

##
## CMS routes
##

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    if path in ('', '/'):
        path = "_index"
    if path.endswith('/'):
        path = path[:-1]
    page = app.config["blog"].pages.get(path)
    if page:
        page.content = page.load_content()
        #print(f"template:\n\tpage: {page.template}\n\turl: {tmanager}")
        #print(str(page.__dict__))

        print(f"get/load plugins ... for page: {page.slug}")
        try:
            pluginmanager.find_plugin(page.items['plugins'])
        except KeyError:
            pass # no plugin in page config
        #TODO if not page.items['plugins'] : save file in cache as static

        context = {
            'page': page,
            'site': app.config["blog"].site,
        }
        context = pluginmanager.injectplugins(context)
        page.content = page.render(**context)
        return render_template(
            templatemanager.TemplateManager(app.root_path).get(
                request.args.get('tpl', page.template)
            ),
            **context
            )
    else:
        #abort(404)

        # 404 if not in *.md
        # page for only dev ;)

        print(f"404: {path}")
        tmp = ""
        for _, page in app.config["blog"].pages.items():
            tmp = tmp +f"<br><a href=\"/{page.slug}\">{page.slug}</a> : {page.title}"
        return (
            f"<h1>404</h1>You want path: <strong>{path}</strong>, page: {page}<hr>"
            f"urls:<br>{tmp}"
            f"<hr><h4>We can change/test templates by add to url <b>?tpl=test.html</b></h4>"
            f"<h4><a href=\"/raz\">RELOAD contents</a></h4>"
            f"<hr>{app.config['blog'].pages.keys()}"
            ), 404


if __name__ == '__main__':
    app.run()
